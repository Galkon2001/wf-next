import "destyle.css";
//import "../static/css/bootstrap.min.css"
import { createGlobalStyle } from "styled-components";
import { COLORS } from "../utils";

const GlobalStyles = createGlobalStyle`
  html, body {
    height: 100%;
    min-height: 100%;
  }

  #__next {
    height: 100%;
  }

  body {
    font-family: 'Avenir Next', sans-serif;
    font-size: 20px;
    font-weight: 400;
    color: ${COLORS.white};
  }
`;

export default GlobalStyles;
