const FontsStyles = () => (
  <style jsx="true" global="true">
    {`
      @font-face {
        font-family: "Avenir Next Cyr";
        src: url("/static/fonts/AvenirNextCyr/AvenirNextCyr-Bold.eot");
        src: url("/static/fonts/AvenirNextCyr/AvenirNextCyr-Bold.eot?#iefix") format("embedded-opentype"),
          url("/static/fonts/AvenirNextCyr/AvenirNextCyr-Bold.woff2") format("woff2"),
          url("/static/fonts/AvenirNextCyr/AvenirNextCyr-Bold.woff") format("woff"),
          url("/static/fonts/AvenirNextCyr/AvenirNextCyr-Bold.svg#AvenirNextCyr-Bold") format("svg");
        font-weight: 700;
        font-style: normal;
      }
      @font-face {
        font-family: "Avenir Next Cyr";
        src: url("/static/fonts/AvenirNextCyr/AvenirNextCyr-Regular.eot");
        src: url("/static/fonts/AvenirNextCyr/AvenirNextCyr-Regular.eot?#iefix") format("embedded-opentype"),
          url("/static/fonts/AvenirNextCyr/AvenirNextCyr-Regular.woff2") format("woff2"),
          url("/static/fonts/AvenirNextCyr/AvenirNextCyr-Regular.woff") format("woff"),
          url("/static/fonts/AvenirNextCyr/AvenirNextCyr-Regular.svg#AvenirNextCyr-Regular") format("svg");
        font-weight: 400;
        font-style: normal;
      }
      @font-face {
        font-family: "Avenir Next Cyr";
        src: url("/static/fonts/AvenirNextCyr/AvenirNextCyr-Demi.eot");
        src: url("/static/fonts/AvenirNextCyr/AvenirNextCyr-Demi.eot?#iefix") format("embedded-opentype"),
          url("/static/fonts/AvenirNextCyr/AvenirNextCyr-Demi.woff2") format("woff2"),
          url("/static/fonts/AvenirNextCyr/AvenirNextCyr-Demi.woff") format("woff"),
          url("/static/fonts/AvenirNextCyr/AvenirNextCyr-Demi.svg#AvenirNextCyr-Demi") format("svg");
        font-weight: 600;
        font-style: normal;
      }
      @font-face {
        font-family: "Avenir Next";
        src: url("/static/fonts/AvenirNext/AvenirNext-Bold.eot");
        src: url("/static/fonts/AvenirNext/AvenirNext-Bold.eot?#iefix") format("embedded-opentype"),
          url("/static/fonts/AvenirNext/AvenirNext-Bold.woff2") format("woff2"),
          url("/static/fonts/AvenirNext/AvenirNext-Bold.woff") format("woff"),
          url("/static/fonts/AvenirNext/AvenirNext-Bold.svg#AvenirNext-Bold") format("svg");
        font-weight: 700;
        font-style: normal;
      }
      @font-face {
        font-family: "Avenir Next";
        src: url("/static/fonts/AvenirNext/AvenirNext-Regular.eot");
        src: url("/static/fonts/AvenirNext/AvenirNext-Regular.eot?#iefix") format("embedded-opentype"),
          url("/static/fonts/AvenirNext/AvenirNext-Regular.woff2") format("woff2"),
          url("/static/fonts/AvenirNext/AvenirNext-Regular.woff") format("woff"),
          url("/static/fonts/AvenirNext/AvenirNext-Regular.svg#AvenirNext-Regular") format("svg");
        font-weight: 400;
        font-style: normal;
      }
      @font-face {
        font-family: "Avenir Next";
        src: url("/static/fonts/AvenirNext/AvenirNext-DemiBold.eot");
        src: url("/static/fonts/AvenirNext/AvenirNext-DemiBold.eot?#iefix") format("embedded-opentype"),
          url("/static/fonts/AvenirNext/AvenirNext-DemiBold.woff2") format("woff2"),
          url("/static/fonts/AvenirNext/AvenirNext-DemiBold.woff") format("woff"),
          url("/static/fonts/AvenirNext/AvenirNext-DemiBold.svg#AvenirNext-DemiBold") format("svg");
        font-weight: 600;
        font-style: normal;
      }
    `}
  </style>
);

export default FontsStyles;
