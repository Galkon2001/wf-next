import { string, object } from "yup";

export default {
  "Sign up": object().shape({
    name: string()
      .required("Required")
      .min(2, "username should have at least 2 symbols")
      .max(30, "Maximum symbols is 30")
      .matches(/^[a-zA-Z0-9!+\-#`.'_ ]+$/, "use only letter, digit and symbols(`.'_!-+)"),
    password: string()
      .required("Required")
      .min(6, "Password should have at least 6 symbols")
      .matches(
        /(?=.*\d)(?=.*[a-zA-Z])/, //(?=.*[-+_!@#$%^&*.,?])
        "Password should have at least 1 digit, 1 letter" //and 1 symbol
      ),
    email: string()
      .required("Required")
      .email("Email is incorrect")
  }),
  "Sign in": object().shape({
    email: string()
      .required("Required")
      .matches(
        /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/, 
        "write existing email"
      )
      .email("Email is incorrect"),
    password: string()
      .required("Required")
      .min(6, "Password should have at least 6 symbols")
      .matches(
        /(?=.*\d)(?=.*[a-zA-Z])/, //(?=.*[-+_!@#$%^&*.,?])
        "Password should have at least 1 digit, 1 letter" //and 1 symbol
      )
  }),
  "Forgot password": object().shape({
    email: string()
      .required("Required")
      .matches(
        /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/, 
        "write existing email" //and 1 symbol
      )
      .email("Email is incorrect")
  }),
  "New password": object().shape({
    password: string()
      .required("Required")
      .min(6, "Password should have at least 6 symbols")
      .matches(
        /(?=.*\d)(?=.*[a-zA-Z])/, //(?=.*[-+_!@#$%^&*.,?])
        "Password should have at least 1 digit, 1 letter" //and 1 symbol
      )
  })
};
