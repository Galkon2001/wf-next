import React, { useState, useEffect, useRef } from 'react';
import styled, { css } from 'styled-components';
import { Header, Icons } from '../../components';
import { constCss } from '../../const';
import Form from './authForm.js';
import { media } from '../../utils';

const Login = props => {
  const [formSent, setFormSent] = useState(false);
  const recaptcha = useRef(null);
  const onResolved = () => {
    setFormSent(true);
    console.log(formSent);
  };

  return (
    <Section>
      {/* <Header /> */}
      <Side left>
        <Container>
          <Title>{/* <Icons.GVA /> */}</Title>
          <Word>FLOATING</Word>
          <Word>OFFSHORE</Word>
          <Word>DESIGNS</Word>
        </Container>
      </Side>
      <Side>
        {/* <Logo src={`/static/images/header/logo_b.png`} /> */}
        {formSent ? <div>Message Sent!</div> : null}
        <Container forma>
          <Form {...props} />
        </Container>
      </Side>
    </Section>
  );
};

export default Login;

const Section = styled.div`
  height: 100vh;
  display: flex;
  /* ${media.tablet`
    flex-direction: column;
    align-items: center;
    height: 200vh;
  `} */
`;

const Side = styled.div`
  position: relative;
  width: 50%; 
  height: 100%;
  ${({ left }) =>
    left &&
    css`
      background: url(/static/images/Home_title_bg.png) no-repeat;
      background-size: cover;
    `}
  ${media.auth1200`
    width: 60%; 
    ${({ left }) =>
      left &&
      css`
        width: 40%;
      `};
  `}
  ${media.authTablet`
    width: 100%;
    ${({ left }) =>
      left &&
      css`
        display: none;
      `};
  `}
`;
const Container = styled.div`
  /* width: 90%;
  padding-left: 15%;
  margin-right: 5%;
  position: absolute;
  top: 50%;
  transform: translateY(-50%);
  ${media.authTablet`
    ${({ forma }) =>
      forma &&
      css`
        padding-left: 8%;
        padding-right: 8%;
        width: 100%;
      `}
  position: relative;
  top: 0;
  transform: translateY(0);
  `} */
`;

const Logo = styled.img`
  display: none;
  ${media.authTablet`
    display: block;
    position: absolute;
    top: 40px;
    left: 10%;
    z-index: 10;
  `}
  ${media.mobile`
    top: 10px;
    left: 10px;
    z-index: 10;    
    width: 65px;
  
  `}
`;

const Title = styled.div`
  margin-bottom: 50px;
  svg {
    height: 110px;
    width: 260px;
  }
  ${media.authTablet`
    display: none;
  `}
  ${media.mobile`
    svg {
      height: 88px;
      width: 208px;
    }
  `}
`;
const Word = styled.div`
  font-size: 50px;
  font-weight: 700;
  line-height: 60px;
  ${media.mobile`
    font-size: 40px;
    font-weight: 700;
    line-height: 48px;
  `}
`;
