import React, { useState, useEffect, useRef } from 'react';
import { connect } from 'react-redux';
import styled, { css } from 'styled-components';
import { Formik } from 'formik';
import schema from './authFormValidationSchema';
import Router from 'next/router';
import { media, COLORS } from '../../utils';
import { toast } from 'react-toastify';

const FORGOT = 'Forgot password';
const NEW_PASSWORD = 'New password';
const SIGNIN = 'Sign in';
const SIGNUP = 'Sign up';

const AuthForm = ({
  fields = { name: '', password: '', email: '' },
  signup,
  signin,
  restore,
  setNewPassword,
  onClose,
  isSignin,
  isSignup,
  isForgot,
  isNewPass
}) => {
  // console.log(signup, signin, restore);
  // const [step, setStep] = useState(subPage || SIGNUP);
  // const isSignup = step === SIGNUP;
  // const isSignin = step === SIGNIN;
  // const isForgot = step === FORGOT;
  // const isNewPass = step === NEW_PASSWORD;
  const step = isSignin ? SIGNIN : isSignup ? SIGNUP : isForgot ? FORGOT : NEW_PASSWORD;

  useEffect(() => {
    const message = Router.router.query.message;
    // console.log(message);
    if (message) {
      toast.error(message, {
        position: toast.POSITION.TOP_RIGHT
      });
      Router.push('/signin');
    }
  }, []);

  return (
    <Container onClose={onClose}>
      <Title isForgot={isForgot} isNewPass={isNewPass}>
        {isForgot && (
          <>
            <TitleSpan isForgot={isForgot} active={isForgot}>
              Forgot password
            </TitleSpan>
            <PasswordText>
              Enter the email address you used when you joined and we’ll send you
              instructions to reset your password.
            </PasswordText>
          </>
        )}
        {isNewPass && <TitleSpan>New password</TitleSpan>}
        {(isSignin || isSignup) && (
          <>
            <TitleSpan active={isSignin} onClick={() => Router.push('/signin')}>
              Log in
            </TitleSpan>
            <TitleSpan active={isSignup} onClick={() => Router.push('/signup')}>
              Sign up
            </TitleSpan>
          </>
        )}
      </Title>
      <Formik
        initialValues={fields}
        validationSchema={schema[step]}
        onSubmit={async (values, actions) => {
          isSignup && (await signup(values));
          isSignin && (await signin({ email: values.email, password: values.password }));
          isForgot && (await restore({ email: values.email }));
          if (isNewPass) {
            //console.log(Router);
            const token = Router.router.query.token;
            //console.log(token);

            await setNewPassword({ password: values.password, token });
          }
          actions.setSubmitting(false);
          //actions.setErrors(data.error);
          //actions.setStatus({ msg: data.error });
          // MyImaginaryRestApiCall(values).then(
          //   updatedField => {
          //     actions.setSubmitting(false);
          //     uodateFields(updatedField);
          //     onClose();
          //   },
          //   error => {
          //     actions.setSubmitting(false);
          //     actions.setErrors(transformMyRestApiErrorsToAnObject(error));
          //     actions.setStatus({ msg: "Set some arbitrary status or data" });
          //   }
          // );
        }}
        render={({
          values,
          errors,
          status,
          touched,
          handleBlur,
          handleChange,
          handleSubmit,
          isSubmitting
        }) => (
          <Form
            isSignin={isSignin}
            isSignup={isSignup}
            isForgot={isForgot}
            isNewPass={isNewPass}
            onSubmit={handleSubmit}
          >
            {/* margin={ isSignin? "100px" : isSignup? "50px" : "52px"} */}
            {isSignup && (
              <Input
                type="name"
                name="name"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.name}
                error={errors.name}
                placeholder={'Name'}
              />
            )}
            {!isNewPass && (
              <Input
                type="email"
                name="email"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.email}
                error={errors.email}
                placeholder={'Email'}
              />
            )}
            {!isForgot && (
              <Input
                type="password"
                name="password"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.password}
                error={errors.password}
                placeholder={'Password'}
                //forgot={isSignin}
                // handleClick={() => Router.push("/forgotpass")}
                isSignin={isSignin}
                isSignup={isSignup}
              />
            )}
            {/* {status && status.msg && <div>{status.msg}</div>} */}
            <Sendbutton type="submit" disabled={isSubmitting}>
              {isNewPass ? 'save' : 'send'}
            </Sendbutton>
          </Form>
        )}
      />
    </Container>
  );
};

const Container = styled.div`
  color: ${COLORS.outerSpace};
  ${media.tablet`
    display: flex;
    flex-direction: column;
    align-items: center;
  `}
`;

const Title = styled.div`
  max-width: 460px;
  ${media.tablet`
    ${({ isForgot, isNewPass }) =>
      (isForgot || isNewPass) &&
      css`
        display: flex;
        flex-direction: column;
        align-items: center;
        /* padding-left: 5%;
      padding-right: 5%; */
      `};
  `}
  ${media.mobile`
    display: flex;
    flex-direction: column;
    align-items: center;
  `}
`;
// const TitleSpan = styled.span`
const TitleSpan = styled.div`
  opacity: ${({ active }) => (active ? 1 : 0.2)};
  font-weight: 700;
  font-size: 50px;
  line-height: 60px;
  color: ${COLORS.darkBlue};
  :first-child {
    margin-right: 50px;
  }
  ${media.tablet`
    ${({ isForgot }) =>
      isForgot &&
      css`
     :first-child {
      margin-right: 0;
      /* font-size: 45px;
      line-height: 56px; */
    `};
  `}
  ${media.mobile`
    :first-child {
      margin-right: 0;
      margin-bottom: 20px;
    }
  `}
`;
const PasswordText = styled.p`
  max-width: 460px;
  margin-top: 100px;
  font-size: 16px;
  line-height: 22px;
  color: ${COLORS.darkBlue};
`;
const Form = styled.form`
  /* margin-top: ${({ margin }) => margin || '0'}; */
  margin-top: ${({ isSignin, isSignup }) =>
    isSignin ? '100px' : isSignup ? '50px' : '52px'};
  width: 100%;
  max-width: 460px;
  ${media.tablet`
    width: 100%;
    padding-left: 5%;
    padding-right: 5%;
  `}
`;
const InputContainer = styled.div`
  position: relative;
`;
const InputField = styled.input`
  font-weight: 600;
  width: 100%;
  margin: 32px 0;
  padding: 20px 0 20px 30px;
  font-size: 16px;
  line-height: 20px;
  border-left: 2px solid white;
  border-radius: 2px;
  outline: none;

  background-color: ${COLORS.lightLightGray};
  ::placeholder {
    color: ${COLORS.regentGray};
  }
  :focus {
    border-left-color: black;
    color: ${COLORS.darkBlue};
    background-color: ${COLORS.white};
  }
`;
const Sendbutton = styled.button`
  font-weight: 600;
  margin-top: 18px;
  padding: 20px 50px;
  font-size: 14px;
  line-height: 20px;
  text-transform: uppercase;
  border-radius: 2px;
  color: ${COLORS.white};
  background-color: ${COLORS.tangaroa};
`;
const ErrorContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
`;
const ErrorMessage = styled.div`
  position: absolute;
  color: red;
  font-size: 16px;
  bottom: 5px;
`;
const PlaceholderContainer = styled.div`
  position: absolute;
  top: 2px;
`;
const Placeholder = styled.div`
  font-size: 16px;
  line-height: 22px;
  color: ${COLORS.regentGray};
`;
const ForgotHref = styled.div`
  position: absolute;
  cursor: pointer;
  font-size: 16px;
  line-height: 22px;
  bottom: 100px;
  right: 0;
`;
const SignupHref = styled.div`
  position: absolute;
  cursor: pointer;
  font-size: 16px;
  line-height: 22px;
  bottom: -55px;
  right: 0;
  ${media.mobile`
    left: 0;
    bottom: -130px;
  `}
`;
const SigninHref = styled.div`
  position: absolute;
  cursor: pointer;
  font-size: 16px;
  line-height: 22px;
  bottom: -55px;
  right: 0;
  ${media.mobile`
    left: 0;
    bottom: -130px;
  `}
`;
const mapDispatch = ({ auth }) => ({
  signup: auth.signup,
  signin: auth.signin,
  restore: auth.restore,
  setNewPassword: auth.setNewPassword
});
export default connect(null, mapDispatch)(AuthForm);

const Input = props => {
  return (
    <InputContainer>
      <InputField {...props} />
      <ErrorContainer>
        <ErrorMessage>{props.error}</ErrorMessage>
        {(props.isSignin && (
          <>
            <ForgotHref onClick={() => Router.push('/forgotpass')}>
              Forgot password?
            </ForgotHref>
            <SigninHref onClick={() => Router.push('/signup')}>
              Don't have an account yet?
            </SigninHref>
          </>
        )) ||
          (props.isSignup && (
            <SignupHref onClick={() => Router.push('/signin')}>
              Already a member?
            </SignupHref>
          ))}
      </ErrorContainer>
      <PlaceholderContainer>
        <Placeholder>{props.placeholder}</Placeholder>
      </PlaceholderContainer>
    </InputContainer>
  );
};
