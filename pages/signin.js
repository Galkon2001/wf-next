import { Login } from '../containers';
import Head from 'next/head';

const SigninPage = () => (
  <div>
    <Head>
      <title>Sign in</title>
    </Head>
    <Login isSignin />;
  </div>
);

export default SigninPage;
