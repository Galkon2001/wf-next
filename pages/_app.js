import App from 'next/app';
import React, { setState } from 'react';
import _get from 'lodash.get';
import styled from 'styled-components';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import store from '../store/store';
import withRematch from '../store/withRematch';
import FontsStyles from '../styles/fonts';
import GlobalStyles from '../styles';

import { mediaJS } from '../utils';

//import { ThemeProvider } from 'styled-components'

class MyApp extends App {
  state = {
    isMobile: typeof window !== 'undefined' && mediaJS.mobile()
  };

  setMobile(flag) {
    setState({ isMobile: flag });
  }

  static async getInitialProps({ ctx }) {
    //adding initial props isAuth
    //console.log(ctx.req.session);
    console.log('-------------------------------', _get(ctx, 'req.session.user', null));
    const pageProps = {
      // _get need for develop
      // isAuth: _get(ctx, "req.session.isAuth", null),
      token: _get(ctx, 'res.locals.csrfToken', null),
      projects: _get(ctx, 'req.session.user', null)
    };
    return { pageProps };
  }
  componentDidMount() {
    const { pageProps, dispatch } = this.props;
    console.log(pageProps);
    dispatch.authToken.setAuthToken(pageProps.token);
    // this.setMobile(mediaJS.mobile());
    // console.log("pageProps2", pageProps);
  }
  render() {
    //console.log("this.props", this.props);
    const { Component, pageProps } = this.props;
    return (
      <Container>
        <GlobalStyles />
        <FontsStyles />
        <Component pageProps={pageProps} isMobileVersion={this.state.isMobile} />
        <ToastContainer />
      </Container>
    );
  }
}

const Container = styled.div``;
export default withRematch(store)(MyApp);
