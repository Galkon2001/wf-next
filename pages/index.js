import React, { useState, useEffect } from 'react';
import Head from 'next/head';
import styled from 'styled-components';
import axios from 'axios';
import { Header, Footer, Icons } from '../components';
import { media, COLORS } from '../utils';
import { constCss } from '../const';

const content = [
  {
    title: 'Production',
    text:
      'The GVA Production Semis-submersible is the leading design for Floating Production units. Sizes ranges from GVA 4500 to GVA 40000.',
    images: [
      '/static/images/homepage/01_production.png',
      '/static/images/homepage/01_production s.png'
    ]
  },
  {
    title: 'Drilling',
    text:
      'The GVA drilling semisubmersible series was revolutionary when first introduced. Today, GVA drilling semisubmersibles are in service worldwide, across a wide range of operational environments.',
    images: ['/static/images/homepage/02_Drilling.png']
  },
  {
    title: 'Wind',
    text:
      'The GVA floating wind farm platform have been developed based on GVA track record of offshore marine design and Semis Submersible in general.',
    images: [
      '/static/images/homepage/03_Wind.png',
      '/static/images/homepage/03_Wind s.png'
    ]
  },
  {
    title: 'FPSO',
    text:
      'The GVA’s FPSOs Platforms complement our semisubmersibles. A design reflecting our many years of yard and operational experience in Brazil and North Sea.',
    images: ['/static/images/homepage/04_FPSO.png']
  },
  {
    title: 'Accommodation and Heavy Lift',
    text:
      'GVA semisubmersible accommodation units proven safe and reliable operations under severe weather conditions have made it the dominating design in the industry',
    images: [
      '/static/images/homepage/05_Accomodation.png',
      '/static/images/homepage/05_Accommodation s.png'
    ]
  }
];

const Home = props => {
  return (
    <>
      <Head>
        <title>Home</title>
      </Head>
      <Container>
        <Comming>Comming soon...</Comming>
        {/* <Header />
        <HomeTitle>
          <Title />
          <TitleName>OFFSHORE PLATFORMS</TitleName>
          <TitleText>The GVA Offshore platforms are developed by KBR Consulting</TitleText>
        </HomeTitle>
        <HomeContent>
          {content.map((el, ind) => (
            <ContentItem reverse={ind % 2} key={ind}>
              <Side left={!(ind % 2)}>
                <ItemTitle>{el.title}</ItemTitle>
                <ItemText>{el.text}</ItemText>
              </Side>
              <Side left={ind % 2} img>
                {el.images.map((img, i) => (
                  <ItemImg key={i} src={img}></ItemImg>
                ))}
              </Side>
            </ContentItem>
          ))}
        </HomeContent>
        <Footer /> */}
      </Container>
    </>
  );
};

const Container = styled.div`
  display: flex;
  /* flex-direction: column; */

  /* only for 'Comming soon' */
  justify-content: center;

  background-color: ${COLORS.tiber};
  min-height: 100vh;
`;

const Comming = styled.div`
  font-size: 27px;
  margin: 200px 100px;
`;

const HomeContent = styled.div`
  ${constCss.container}
  padding-bottom: 100px;
  ${media.mobile`
    padding-bottom: 50px;
    }
  `}
`;
const HomeTitle = styled.div`
  background: url(/static/images/Home_title_bg.png) no-repeat;
  background-size: cover;
  padding: 215px 0;
  text-align: center;
  ${media.mobile`
    svg{
      width: 250px;
      height: 106px; 
    }
  `}
`;
const Title = styled(Icons.GVA)``;
const TitleName = styled.h2`
  ${constCss.offsets}
  font-size: 50px;
  font-weight: 700;
  margin: 40px 0 15px;
  ${media.mobile`  
    font-size: 40px;
    font-weight: 700;
  `}
`;
const TitleText = styled.h3`
  ${constCss.offsets}
  font-size: 20px;
`;
const ContentItem = styled.div`
  ${constCss.offsets}
  display: flex;
  justify-content: center;
  flex-direction: ${({ reverse }) => (reverse ? 'row-reverse' : 'initial')};
  margin-bottom: 100px;
  ${media.tablet`
    flex-direction: column;
    align-items: center;
  `}
  ${media.mobile`
    padding-left: 0;
    padding-right: 0;
    margin-bottom: ${({ reverse }) => (reverse ? '60px' : '160px')};
  `}
`;
const Side = styled.div`
  width: 50%;
  padding: ${({ left }) => (left ? '0 15px 0 0' : '0 0 0 15px')};
  text-align: ${({ left }) => (left ? 'right' : 'left')};
  position: relative;
  ${media.tablet`
    width: 80%;
    padding: 0;
    text-align: center;
    position: relative;
  `}
`;
const ItemTitle = styled.div`
  font-size: 50px;
  font-weight: 700;
  line-height: 60px;
  ${media.tablet`
    text-align: center;
  `}
  ${media.mobile`
  font-size: 30px;
  font-weight: 700;
  line-height: 48px;
  `}
`;
const ItemText = styled.div`
  line-height: 100%;
  line-height: 30px;
  margin-top: 20px;
  ${media.tablet`
    margin-bottom: 20px;
  `}
`;
const ItemImg = styled.img`
  & :nth-child(1) {
    ${media.mobile`
    width: 250px;
  `}
  }
  & :nth-child(2) {
    position: absolute;
    bottom: -20%;
    left: 275px;
    ${media.tablet`
      left: calc(50% + 55px);
    `}
    ${media.mobile`
      position: absolute;
      bottom: -60%;
      left: calc(50% - 100px);
    `}
  }
`;

export default Home;
