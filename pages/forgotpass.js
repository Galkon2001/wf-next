import { Login } from "../containers";

const ForgotPage = () => <Login isForgot />;

export default ForgotPage;
