import React, { useState, useEffect } from "react";
import Head from "next/head";
import styled, { css } from "styled-components";
import { Footer, Icons } from "../components";
import { media, COLORS, Req } from "../utils";
import { connect } from "react-redux";

const AdminPage = ({ initUserList, getUsers, setProjects, setLogout, ...props }) => {
  
  const [userList, setChecked] = useState(null);

  const handleSubmit = () => {
    setProjects(userList);
  }

  const handleChange = ({
    target: {
      dataset: {
        email: datasetEmail,
        project: datasetProject,
      }
    }
  }) => {
    const updatedUserList = userList.map((item) => {
      if (item.email === datasetEmail && datasetProject in item.projects) {
        item.projects[datasetProject] = !item.projects[datasetProject];
      }
      return item;
    });

    setChecked(updatedUserList);
  }

  const handleAddAll = ({ 
    currentTarget : {
      dataset : { 
        email: datasetEmail, 
      }
    }
  }) => {
    const updatedUserList = userList.map((item) => {
      if (item.email === datasetEmail) {
        for (let project in item.projects) {
          item.projects[project] = true;
        }
      }
      return item;
    });
    
    setChecked([...updatedUserList]);
  }

  const handleLogout = () => {
    setLogout();
  }

  useEffect(() => { 
    if (!initUserList) {
      getUsers();
    }   
      setChecked(initUserList);
  }, [initUserList, userList])

  //console.log(userList);

  return Array.isArray(userList) ? (
    <>
      <Head>
        <title>Admin</title>
      </Head>
      <Container>
        <Header>
          <Logo src={`./static/images/header/logo_b.png`} />
          <Logout onClick={handleLogout}>Log Out</Logout>
          {/* src={`./static/images/admin/chat.svg`} /> */}
        </Header>
        <AdminContent>
          <AdminTitle>Access to projects</AdminTitle>
          {userList.map((item) => {            
            return (
              <ContentItem className="ContentItem" key={item._id}>
                <ItemTitle>
                  <ItemUser>{item.email}</ItemUser>
                  <ItemAddAll
                    onClick={handleAddAll}
                    data-email={item.email}
                  >
                    <AddAllIcon src="./static/images/admin/add.svg" />
                    <AddAllText>Add All</AddAllText>
                  </ItemAddAll>
                </ItemTitle>
                <ItemTable>
                  {Object.entries(item.projects).map((entry, id) =>
                    <ProjectItem key={id}>
                      <ProjectInputLabel>
                        <ProjectInput
                          className="ProjectInput"
                          type="checkbox"
                          // defaultChecked={entry[1]}
                          onChange={() => {}}
                          checked={entry[1]}
                          onClick={handleChange}
                          data-email={item.email}
                          data-project={entry[0]}
                        >
                        </ProjectInput>
                        {/* <ProjectInput type="checkbox" defaultChecked={entry[1]} /> */}
                        <ProjectCheckmark src="./static/images/admin/check.svg" />
                      </ProjectInputLabel>
                      <ProjectNumber>{entry[0]}</ProjectNumber>
                    </ProjectItem>
                  )}
                </ItemTable>
              </ContentItem>
            )
          })}
          <SaveButton onClick={handleSubmit}>Save</SaveButton>
        </AdminContent>
      </Container>
      <Footer></Footer>
    </>
  ) : null
}

const Container = styled.div`
    position: relative;
    display: flex;
    flex-direction: column;
    align-items: center;
    min-height: 100vh;
    width: 100%;
`;
const Header = styled.div`
`;
const Logo = styled.img`
    position: absolute;
    top: 33px;
    left: 60px;
`;
const Logout = styled.button`
    position: absolute;
    top: 45px;
    right: 55px;
    color: ${COLORS.darkBlue};
    font-weight: 600;
    font-size: 18px;
    line-height: 22px; 
    opacity: 0.5;
    & :hover {
        cursor: pointer;
        opacity: 1;      
    }
`;
const Chat = styled.img``;
const AdminTitle = styled.h1`  
    margin-top: 150px;
    color: ${COLORS.darkBlue};
    font-weight: 700;
    font-size: 50px;
    line-height: 60px;
    align-self: flex-start;
    ${media.mobile`        
    `}
`;
const AdminContent = styled.div`
    margin: 0 15%;
    max-width: 980px;
    color: ${COLORS.darkBlue};
    font-weight: 600;
    font-size: 16px;
    line-height: 20px;
`;
const ContentItem = styled.div`   
`;
const ItemTitle = styled.div`
    margin-top: 50px;
    display: flex;
    flex-direction: row; 
    justify-content: space-between;
    ${media.mobile`
        flex-direction: column; 
    `}   
`;
const ItemUser = styled.h3``;
const ItemAddAll = styled.div`
    display: flex;
    opacity: 0.5;
    & :hover {
        cursor: pointer;
        opacity: 1;      
    }
    ${media.mobile`
        margin-top: 10px; 
    `}
`;
const AddAllIcon = styled.img`
    margin-right: 15px;
`;
const AddAllText = styled.div``;
const ItemTable = styled.div`
    display: flex;
    flex-direction: row; 
    justify-content: flex-start;
    flex-wrap: wrap;
    padding-bottom: 50px;
    border-bottom: 1px solid ${COLORS.lightBlue};
`;
const ProjectItem = styled.div`
    margin-top: 30px;
    margin-right: 23px;
    width: 220px;
    height: 60px;
    border: 1px solid ${COLORS.lightBlue};
    border-radius: 2px;
    display: flex;
    flex-direction: row; 
    justify-content: flex-start;
    align-items: center;
    padding: 15px 0;
    padding-left: 30;
`;
const ProjectInputLabel = styled.label`
    width: 30px;
    height: 30px;
    margin-right: 15px;
    margin-left: 30px;
    border-radius: 2px;
    background-color: ${COLORS.lightLightGray};
`;
const ProjectCheckmark = styled.img`
    margin: 7.3px 5px;
    display: none;  
`;
const ProjectInput = styled.input`
    display: none; 
    :checked + ${ProjectCheckmark} {
        display: block;
    }   
`;
const ProjectNumber = styled.div`   
`;
const SaveButton = styled.button`
    font-weight: 700;
    margin-top: 50px;
    margin-bottom: 150px;
    padding: 20px 65.5px;
    font-size: 14px;
    line-height: 20px;
    text-transform: uppercase;
    border-radius: 2px;
    color: ${COLORS.white};
    background-color: ${COLORS.tangaroa};  
`;
const mapState = ({ admin }) => ({
  initUserList: admin.userList,
})
const mapDispatch = ({ admin }) => ({
  getUsers: admin.getUsers,
  setProjects: admin.setProjects,
  setLogout: admin.setLogout
});

export default connect(
  mapState,
  mapDispatch
)(AdminPage);