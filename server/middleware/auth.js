module.exports = function(req, res, next) {
  if (!req.session.isAuth) {
    return res.status(400).json({
      error: { message: "You don`t have permission for this request" }
    });
  }
  next();
};
