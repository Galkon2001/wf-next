require('dotenv').config();
const express = require('express');
const next = require('next');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const session = require('express-session');
const MongoStore = require('connect-mongodb-session')(session);
const csurf = require('csurf');
const dev = process.env.NODE_ENV !== 'production'; //true false
const nextApp = next({ dev });
const handle = nextApp.getRequestHandler(); //part of next config
const Config = require('./config/config');
const cors = require('cors');

//dateParseHelper();
const { MONGODB_URI, PORT, URL } = Config;

const store = new MongoStore({
  collection: 'session',
  uri: MONGODB_URI
});

mongoose
  .connect(MONGODB_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true
  })
  .then(() => {
    try {
      nextApp.prepare().then(async () => {
        // express code here
        const app = express();
        //app.use(credentials);
        app.use(bodyParser.json());
        app.use(bodyParser.urlencoded({ extended: true }));
        app.use(
          session({
            secret: process.env.SESSION_SECRETE,
            resave: false,
            saveUninitialized: false,
            cookie: {
              maxAge: 1000 * 60 * 60 * 24 // 1day
              //sameSite: false,
              //secure: true,
              //domain: 'gva.net',
            },
            store
            // resave: true,
          })
        );
        // app.use(function(req, res, next) {
        //   next();
        // });

        app.use(cors());
        app.use(csurf());

        // app.use(express.static('public'));

        app.use((req, res, next) => {
          res.locals.csrfToken = req.csrfToken();
          next();
        });
        app.use('/api/users', require('./routes/api/users'));
        app.use('/api/auth', require('./routes/api/auth'));
        app.get('/newpass/:token', (req, res) =>
          nextApp.render(req, res, '/newpass', {
            token: req.params.token,
            subPage: 'New password'
          })
        );

        app.get('*', (req, res) => handle(req, res)); // for all the react stuff);
        app.listen(PORT, err => {
          if (err) throw err;
          //console.log(`ready at ${URL}`);
        });
        //https.createServer(httpsOptions, app).listen(443);
      });
    } catch (error) {
      console.log('prepare', error);
    }
  })
  .catch(err => console.log(error));
