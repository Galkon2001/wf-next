const isProd = process.env.NODE_ENV === 'production';
let protocol = '';
let hostname = '';
if (typeof window !== 'undefined') {
  protocol = window.location.protocol;
  hostname = window.location.hostname;
}

console.log('protocol, hostname', protocol, hostname);
const port = `${isProd ? 80 : 3000}`;
const URL = `${protocol}//${hostname}${isProd ? '' : ':3000'}`;

module.exports = {
  MONGODB_URI: 'mongodb://localhost:27017/galina-site',
  API: `${URL}/api`,
  URL,
  SERVER_STATIC_PATH: '/public',
  AUTH_HEADER_NAME: 'CSRF-Token',
  isProd,
  PORT: port
};
