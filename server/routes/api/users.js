const Router = require('express').Router;
const router = Router();

const User = require('../../models/User');

router.get('/', async (req, res) => {
  const users = await User.find({ role: 'user' }, 'email projects username');
  res.status(200).json(users);
});

router.post('/edituseraccess', async (req, res) => {
  try {
    const { userInfo } = req.body;
    await Promise.all(
      userInfo.map(async ({ projects, _id: userID }) => {
        const user = await User.findById(userID);
        user.projects = projects;
        await user.save();
        return true;
      })
    );
    res.status(200).json(`${userInfo.length} users updated`);
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
  }
});
module.exports = router;
