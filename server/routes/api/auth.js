const Router = require('express').Router;
const router = Router();
const bcrypt = require('bcryptjs');
const _pick = require('lodash.pick');
const auth = require('../../middleware/auth');
const User = require('../../models/User');
const nodemailer = require('nodemailer');

const Config = require('../../config/config');
const { URL } = Config;

const hashPassword = async password => await bcrypt.hash(password, 10);
// const hash = async () => {
//   console.log(await hashPassword('iloveyoumydiargalinka888'))
// }
// hash();
const initAdmin = async () => {
  try {
    const admin = await User.findOne({ role: 'admin' }).catch(e =>
      console.log('error', e)
    );
    const password = await hashPassword(process.env.ADMIN_PASSWORD);
    if (admin === null) {
      const user = new User({
        username: process.env.ADMIN_LOGIN,
        role: 'admin',
        password,
        email: process.env.ADMIN_EMAIL,
        projects: {
          project1: true,
          project2: true,
          project3: true,
          project4: true,
          project5: true,
          project6: true
        }
      });
      await user.save();
    }
  } catch (error) {
    console.log(error);
  }
};
initAdmin();

router.post('/signin', async (req, res) => {
  console.log('signin');
  try {
    const { email, password } = req.body;
    const user = await User.findOne({ email });
    if (user) {
      const areSame = await bcrypt.compare(password, user.password);

      if (areSame) {
        // req.session.role = user.role;
        console.log(user);
        req.session.user = _pick(user.toObject(), [
          'language',
          'role',
          'username',
          'email'
        ]);
        req.session.save(err => {
          if (err) {
            throw err;
          }
          // console.log(req.body.name, req.session);
          res.status(200).json({ projects: user.projects });
        });
      } else {
        return res.status(400).json({ error: 'wrong password' });
      }
    } else {
      return res.status(400).json({ error: 'email does not registered' });
    }
  } catch (error) {}
});

router.post('/signup', async (req, res) => {
  const { email, name } = req.body;
  try {
    const test_email = await User.findOne({ email });
    //const test_username = await User.findOne({ username });
    // console.log(test_email);
    if (test_email) {
      return res.status(400).json({
        error: `${test_email ? 'email exist;' : ''}`
      });
    }
    //console.log("register", req.body, req.session);
    const password = await hashPassword(req.body.password);
    const user = new User({
      email,
      username: name,
      password,
      projects: []
    });
    await user.save();
    res.status(200).json('ok!');
  } catch (error) {
    console.log(error);

    return res.status(400).json({ error: error.message });
  }
});

router.post('/restore', async (req, res) => {
  const { email } = req.body;
  try {
    const user = await User.findOne({ email });
    if (!user) {
      return res.status(400).json({
        error: 'no users with this email'
      });
    }
    const token = bcrypt.genSaltSync(10);
    user.resetPasswordToken = token;
    user.resetPasswordExpires = Date.now() + 1000 * 60 * 60;
    await user.save();

    const transporter = nodemailer.createTransport({
      host: 'mail.adm.tools', //smtp.mail.adm.tools
      port: 465,
      secure: true,
      auth: {
        user: process.env.EMAIL_USER2,
        pass: process.env.EMAIL_PASS2
      }
    });
    const mailOptions = {
      from: 'info@unials.com',
      to: user.email,
      subject: 'Link to Reset Password',
      text:
        'You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n' +
        'Please click on the following link, or paste this into your browser to complete the process within one hour of receiving it:\n\n' +
        `${URL}/newpass/${token}\n\n` +
        'If you did not request this, please ignore this email and your password will remain unchanged.\n'
    };
    // console.log("sending mail");

    transporter.sendMail(mailOptions, (err, response) => {
      if (err) {
        console.log(err);
        res.status(500).json({ success: false, message: err.message });
      } else {
        res.status(200).json({ success: true, message: 'recovery email sent', response });
      }
    });
  } catch (error) {
    console.log(error);

    return res.status(400).json({ error: error.message });
  }
});

router.post('/newpass', async (req, res) => {
  const { password, token } = req.body;
  try {
    const user = await User.findOne({
      resetPasswordToken: token
    });
    // console.log(user);
    if (!user) {
      return res.status(400).json({
        error: 'no users found'
      });
    }
    if (user.resetPasswordExpires < Date.now()) {
      // console.log(user.resetPasswordExpires, Date.now());
      // console.log(user.resetPasswordExpires < Date.now());
      return res.status(400).json({
        error: 'password reset link is invalid or has expired'
      });
    }
    delete user.resetPasswordExpires;
    delete user.resetPasswordToken;
    user.password = await hashPassword(password);
    await user.save();
    res.status(200).json('password change successfully');
  } catch (error) {
    console.log(error);
    return res.status(500).json({ error: error.message });
  }
});

router.get('/logout', async (req, res) => {
  req.session.destroy();
  res.status(200).json('ok!');
});

router.get('/', auth, async (req, res) => {
  // console.log(req.session.isAuth);
  res.status(200).json(!!req.session.isAuth);
});

module.exports = router;
