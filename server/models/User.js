const mongoose = require('mongoose');
//import uniqueValidator from 'mongoose-unique-validator';
// import crypto from "crypto";
// import jwt from "jsonwebtoken";
// import { secret } from "../config";
// import { logError } from "../tools";

const emailRegexp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

const UserSchema = new mongoose.Schema({
  username: {
    type: String,
    required: [true, "can't be blank"],
    match: [/^[a-zA-Z0-9!+\-#`.'_ ]+$/, 'is invalid']
  },
  email: {
    type: String,
    lowercase: true,
    required: [true, "can't be blank"],
    match: [emailRegexp, 'choose existing email']
  },
  password: { type: String, required: true },
  language: { type: String, required: true, default: 'ru' },
  projects: {
    project1: { type: Boolean, required: true, default: false },
    project2: { type: Boolean, required: true, default: false },
    project3: { type: Boolean, required: true, default: false },
    project4: { type: Boolean, required: true, default: false },
    project5: { type: Boolean, required: true, default: false },
    project6: { type: Boolean, required: true, default: false }
  },
  role: { type: String, required: true, default: 'user' },
  resetPasswordToken: { type: String, required: false },
  resetPasswordExpires: { type: Date },
  createdDate: { type: Date, required: true, default: Date.now() }
});

module.exports = mongoose.model('User', UserSchema);
