const mongoose = require("mongoose");

const ProjectSchema = new mongoose.Schema({
  userName: {
    name: String,
    required: [true, "can't be blank"]
  },
  email: {
    type: String,
    lowercase: true,
    required: [true, "can't be blank"],
    match: [emailRegexp, "choose existing email"]
  },
  password: { type: String, required: true },
  projects: {
    projectId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Projects"
    }
  },
  role: { type: String, required: true, default: "user" },
  resetPasswordToken: { type: String, required: false },
  resetPasswordExpires: { type: Date }
});
module.exports = mongoose.model("User", UserSchema);
