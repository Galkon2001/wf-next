const initialState = {
  language: 'ru',
  name: 'guest'
};
const user = {
  state: initialState,
  reducers: {
    setUser(state, payload) {
      return { ...state, ...payload };
    }
  },
  effects: dispatch => ({
    // handle state changes with impure functions.
    // use async/await for async actions
    // async signup(payload) {
    //   try {
    //     const data = await Req.post('auth/signup', payload);
    //     toast.success('Account was created successfully', {
    //       position: toast.POSITION.TOP_RIGHT
    //     });
    //     Router.push('/modelviewer');
    //   } catch (error) {
    //     console.log(error);
    //     toast.error(error.message, {
    //       position: toast.POSITION.TOP_RIGHT
    //     });
    //     return { error: error.message };
    //   }
    // },
    // async signin(payload) {
    //   const { setProjects, state } = dispatch.auth;
    //   try {
    //     const { data } = await Req.post('auth/signin', payload);
    //     toast.success('Authorization successful', {
    //       position: toast.POSITION.TOP_RIGHT
    //     });
    //     if (data.projects) {
    //       setProjects(data.projects);
    //     }
    //     Router.push('/modelviewer');
    //   } catch (error) {
    //     console.log(error);
    //     toast.error('Wrong email or password', {
    //       position: toast.POSITION.TOP_RIGHT
    //     });
    //     return { error: error.message };
    //   }
    // },
    // async restore(payload) {
    //   try {
    //     const data = await Req.post('auth/restore', payload);
    //     toast.success('Email was sent successfully', {
    //       position: toast.POSITION.TOP_RIGHT
    //     });
    //     Router.push('/signin');
    //   } catch (error) {
    //     console.log(error);
    //     toast.error(error.message, {
    //       position: toast.POSITION.TOP_RIGHT
    //     });
    //     return { error: error.message };
    //   }
    // },
    // async setNewPassword(payload) {
    //   try {
    //     const data = await Req.post('auth/newpass', payload);
    //     toast.success('Password was changed successfully', {
    //       position: toast.POSITION.TOP_RIGHT
    //     });
    //     Router.push('/signin');
    //   } catch (error) {
    //     console.log(error);
    //     toast.error(error.message, {
    //       position: toast.POSITION.TOP_RIGHT
    //     });
    //     return { error: error.message };
    //   }
    // }
  })
};

export default auth;
