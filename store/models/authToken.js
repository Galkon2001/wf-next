const authToken = {
  state: '',
  reducers: {
    setAuthToken(state, payload) {
      return payload;
    },
    deleteAuthToken() {
      return '';
    }
  }
};

export default authToken;
