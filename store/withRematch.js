import React from "react";
import { connect, Provider } from "react-redux";
// import { getPersistor } from '@rematch/persist';
// import { PersistGate } from 'redux-persist/lib/integration/react';
import { isServer as checkServer } from "../utils";

const __NEXT_REMATCH_STORE__ = "__NEXT_REMATCH_STORE__";

const getOrCreateStore = initStore => {
  // Always make a new store if server
  if (checkServer() || typeof window === "undefined") {
    return initStore;
  }

  // Memoize store in global variable if client
  if (!window[__NEXT_REMATCH_STORE__]) {
    window[__NEXT_REMATCH_STORE__] = initStore;
  }
  return window[__NEXT_REMATCH_STORE__];
};

// const persistor = getPersistor();

export default (...args) => Component => {
  // First argument is initStore, the rest are redux connect arguments and get passed
  const [initStore, ...connectArgs] = args;
  // Connect page to redux with connect arguments
  const ConnectedComponent = connect(...connectArgs)(Component);

  const ComponentWithRematch = (props = {}) => {
    const { store, initialProps, initialState, ...others } = props;

    // Wrap with redux Provider with store
    // Create connected page with initialProps
    return (
      <Provider
        store={
          store && store.dispatch
            ? store
            : getOrCreateStore(initStore, initialState)
        }
      >
        {/* <PersistGate persistor={persistor}> */}
        <ConnectedComponent {...initialProps} {...others} />
        {/* </PersistGate> */}
      </Provider>
    );
  };

  ComponentWithRematch.getInitialProps = async (props = {}) => {
    const isServer = checkServer();
    const store = getOrCreateStore(initStore);

    // Run page getInitialProps with store and isServer
    const initialProps = Component.getInitialProps
      ? await Component.getInitialProps({ ...props, isServer, store })
      : {};

    return {
      store,
      initialState: store.getState(),
      initialProps
    };
  };

  return ComponentWithRematch;
};
