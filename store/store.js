import { init } from '@rematch/core';
import { isProd } from '../server/config/config';
//import createRematchPersist from '@rematch/persist';
import * as models from './models';

// const persistPlugin = createRematchPersist({
//   whitelist: ['sound', 'test'],
//   throttle: 500,
//   version: 1,
// });

const store = init({
  models
  // redux: {
  //   reducers: {
  //     form: formReducer,
  //   },
  // },
  // plugins: [persistPlugin],
});
//loger
// !isProd && store.subscribe(() => console.log(store.getState()));

export default store;
