import React, { useState } from 'react';
import styled from 'styled-components';

const Header = () => {
  return <Container>Head of the site will be here</Container>;
};

export default Header;

const Container = styled.div`
  width: 100%;
  min-height: 50px;
  background-color: lightblue;
  display: flex;
  color: yellow;
  align-items: center;
  justify-content: center;
`;
