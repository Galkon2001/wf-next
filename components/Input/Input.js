import React, { useState } from "react";
import styled from "styled-components";
//import { SvgIcon } from 'components';

const PasswordToogler = ({ setType }) => {
  const [visibility, setVisibility] = useState(false);
  setType(visibility ? "text" : "password");
  return (
    <IconButton type="button" onClick={() => setVisibility(v => !v)}>
      {/* <VisibleIcon /> */}
    </IconButton>
  );
};

const Input = ({
  children,
  tag,
  className,
  label,
  error,
  type = "text",
  ...props
}) => {
  const { disabled } = props;
  const [controlledType, setType] = useState(type);
  return (
    <Container className={className}>
      {(label || error) && (
        <TopLine>
          <Label>{label}</Label>
          <Error>{error}</Error>
        </TopLine>
      )}
      <InputWrapper>
        <InputElement as={tag} type={controlledType} {...props}>
          {children}
        </InputElement>
        {type === "password" && !disabled && (
          <PasswordToogler setType={setType} />
        )}
      </InputWrapper>
    </Container>
  );
};

export default Input;

const Container = styled.label`
  display: block;
  width: 360px;
`;

const InputWrapper = styled.span`
  display: block;
  position: relative;
`;

const InputElement = styled.input`
  width: 100%;
  min-height: 50px;
  background-color: #f5f7f8;
  border-radius: 10px;
  color: #0f161a;
  font-weight: 700;
  padding: 15px 40px;
  font-size: 14px;

  &::placeholder {
    color: #0f161a;
    opacity: 0.5;
  }
`;

export const TopLine = styled.span`
  display: flex;
  justify-content: space-between;
  margin-bottom: 10px;
`;

export const Label = styled.span`
  color: #9cb0c6;
  font-size: 12px;
  font-weight: 700;
`;
export const Error = styled.span`
  color: #cd3131;
  font-size: 12px;
  font-weight: 700;
`;

const IconButton = styled.button`
  position: absolute;
  width: 20px;
  right: 20px;
  top: 0;
  bottom: 0;
`;

// const VisibleIcon = styled(SvgIcon.Eye)`
//   width: 100%;
// `;
