import axios from "axios";
import Сonfig from "../server/config/config.js";
import store from "../store/store";
const { API, AUTH_HEADER_NAME } = Сonfig;

axios.interceptors.request.use(
  reqConf => {
    const token = store.getState().authToken;
    if (token) {
      reqConf.headers[AUTH_HEADER_NAME] = token;
    }
    return reqConf;
  },
  error => Promise.reject(error)
);

//console.log(API);
export default {
  post(route, config) {
    console.log(API);
    console.log(route);
    return axios.post(`${API}/${route}`, config);
  },
  get(route, config) {
    return axios.get(`${API}/${route}`, config);
  },
  getNoAPI(route, config) {
    return axios.get(`${route}`, config);
  },

};
