// Colors named accordingly this app - http://chir.ag/projects/name-that-color

const COLORS = {
  black: "#000",
  white: "#fff",
  tiber: "#0B2F3F",
  outerSpace: "#2f343b",
  regentGray: "#8E98A5",
  lightLightGray: "#F4F6F9",
  darkBlue: "#2F343B",
  tangaroa: "#0C2C3A",
  lightBlue: "#EBEFF5"
  // bostonBlue: "#338ac7",
  // mineShaft: "#333",
  // pictionBlue: "#259ded",
  // gallery: "#eaeaea",
  // cornflowerBlue: "#0979c4",
  // curiousBlue: "#2B94DB",
  // funBlue: "#1861a9",
  // boulder: "#777",
  // silverChalice: "#aaa",
  // dustyGray: "#9B9B9B",
  // elephant: "#10293C",
  // alto: "#ddd",
  // mariner: "#247fbc",
  // silver: "#bbb",
  // codGray: "#111",
  // veniceBlue: "#084974"
};

module.exports = COLORS;
