import { css } from "styled-components";

export default {
  container: css`
    max-width: 1440px;
    margin: 0 auto;
  `,
  offsets: css`
    padding-left: 8.33%;
    padding-right: 8.33%;
  `
};
